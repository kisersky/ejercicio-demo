package model;

import java.time.LocalDate;

public class TarjetaPERE extends Tarjeta {

	private static EMarcaTarjeta marca = EMarcaTarjeta.PERE;

	public TarjetaPERE(Long numero, String cardholder, LocalDate fechaVencimiento) {
		super(marca, numero, cardholder, fechaVencimiento);
	}

	@Override
	public Double calcularTasa() {
		LocalDate hoy = LocalDate.now();
		return hoy.getMonthValue() * 0.1;
	}

}
