package model;

import java.time.LocalDate;

public class TarjetaSCO extends Tarjeta {

	private static EMarcaTarjeta marca = EMarcaTarjeta.SCO;

	public TarjetaSCO(Long numero, String cardholder, LocalDate fechaVencimiento) {
		super(marca, numero, cardholder, fechaVencimiento);
	}

	@Override
	public Double calcularTasa() {
		LocalDate hoy = LocalDate.now();
		return hoy.getDayOfMonth() * 0.5;
	}

}
