package model;

import java.time.LocalDate;

public class TarjetaSQUA extends Tarjeta {

	private static EMarcaTarjeta marca = EMarcaTarjeta.SQUA;

	public TarjetaSQUA(Long numero, String cardholder, LocalDate fechaVencimiento) {
		super(marca, numero, cardholder, fechaVencimiento);
	}

	@Override
	public Double calcularTasa() {
		LocalDate hoy = LocalDate.now();
		return (double) (hoy.getYear() / hoy.getMonthValue());
	}

}
