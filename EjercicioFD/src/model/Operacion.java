package model;

import java.awt.print.PrinterException;
import java.net.ConnectException;
import java.sql.SQLException;

public class Operacion {
	private Tarjeta tarjeta;
	private Double importe;

	public Operacion(Tarjeta tarjeta, Double importe) {
		this.tarjeta = tarjeta;
		this.importe = importe;

		System.out.printf("Se ha generado una nueva operación para una tarjeta %s por un importe de $ %.2f\n",
				this.tarjeta.getMarca(), this.importe);
	}

	public boolean esValida() {
		return this.importe < 1000;
		// no está especificado en el ejercicio pero podría fijarse si la tarjeta es
		// válida también: return this.tarjeta.esValida() && this.importe < 1000;
	}

	public void imprimirInformacion() {

		System.out.println("\n***************************************************");
		System.out.println("*********** INFORMACIÓN DE LA OPERACIÓN ***********");
		System.out.println("");
		System.out.printf("	· Marca:	%s\n", this.tarjeta.getMarca());
		System.out.printf("	· Subtotal:	$ %.2f\n", this.importe);
		System.out.printf("	· Tasa:		%.2f%%\n", this.tarjeta.calcularTasa());
		System.out.printf("	· Total:	$ %.2f\n", obtenerImporteConTasa());
		System.out.println("");
		System.out.println("***************************************************\n");
	}

	public Double obtenerImporteConTasa() {
		Double tasa = this.tarjeta.calcularTasa();
		Double importeFinal = this.importe * (1 + tasa / 100);

		return importeFinal;
	}

	public void cobrar() {
		try {
			imprimirFactura();
		} catch (PrinterException e) {
			System.out.printf("Ocurrió un error en la impresión de la factura. Motivo: %s.\n", e.getMessage());
		}

		try {
			enviarInfoTC();
		} catch (ConnectException e) {
			System.out.printf("Ocurrió un error en la conexión al enviar los datos de la TC. Motivo: %s.\n",
					e.getMessage());
		}

		try {
			informarPago();
		} catch (ConnectException e) {
			System.out.printf("Ocurrió un error en la conexión al informar el pago. Motivo: %s.\n", e.getMessage());
		}

		try {
			actualizarSaldo(new Cliente());
		} catch (SQLException e) {
			System.out.printf(
					"Ocurrió un error al intentar actualizar el saldo del cliente en la base de datos. Motivo: %s.\n",
					e.getMessage());
		}
	}

	public void imprimirFactura() throws PrinterException {
		System.out.println("Imprimiendo factura...");
		throw new PrinterException("Papel atascado");
	}

	public void enviarInfoTC() throws ConnectException {
		System.out.println("Enviando información de la tarjeta de crédito...");
	}

	public void informarPago() throws ConnectException {
		System.out.println("Informando pago a comercial...");
	}

	public void actualizarSaldo(Cliente cliente) throws SQLException {
		System.out.println("Actualizando saldo del cliente...");
	}

	public Tarjeta getTarjeta() {
		return tarjeta;
	}

	public void setTarjeta(Tarjeta tarjeta) {
		this.tarjeta = tarjeta;
	}

	public Double getImporte() {
		return importe;
	}

	public void setImporte(Double importe) {
		this.importe = importe;
	}

}
