package model;

import java.time.LocalDate;

public abstract class Tarjeta implements CalculoTasa {

	private EMarcaTarjeta marca;
	private Long numero;
	private String cardholder;
	private LocalDate fechaVencimiento;

	public Tarjeta(EMarcaTarjeta marca, Long numero, String cardholder, LocalDate fechaVencimiento) {
		this.marca = marca;
		this.numero = numero;
		this.cardholder = cardholder;
		this.fechaVencimiento = fechaVencimiento;

		System.out.printf("Se ha generado una nueva tarjeta marca %s\n", this.marca);
	}

	public Double calcularTasa() {
		return 0.0;
	}

	public void imprimirInformacion() {
		System.out.println("\n***************************************************");
		System.out.println("************ INFORMACIÓN DE LA TARJETA ************");
		System.out.println("");
		System.out.printf("	· Marca:		%s\n", this.marca);
		System.out.printf("	· Numero:		%s\n", this.numero);
		System.out.printf("	· Cardholder:		%s\n", this.cardholder);
		System.out.printf("	· Fec. Venc.:		%d/%d\n", this.fechaVencimiento.getMonthValue(),
				this.getFechaVencimiento().getYear());
		System.out.println("");
		System.out.println("***************************************************\n");
	}

	public boolean esValida() {
		LocalDate hoy = LocalDate.now();
		return hoy.isBefore(this.fechaVencimiento);
	}

	public boolean esDistintaA(Tarjeta otraTarjeta) {
		return !(this.marca.equals(otraTarjeta.getMarca()) && this.numero.equals(otraTarjeta.getNumero())
				&& this.cardholder.equals(otraTarjeta.getCardholder())
				&& this.fechaVencimiento.equals(otraTarjeta.getFechaVencimiento()));
	}

	public EMarcaTarjeta getMarca() {
		return marca;
	}

	public void setMarca(EMarcaTarjeta marca) {
		this.marca = marca;
	}

	public Long getNumero() {
		return numero;
	}

	public void setNumero(Long numero) {
		this.numero = numero;
	}

	public String getCardholder() {
		return cardholder;
	}

	public void setCardholder(String cardholder) {
		this.cardholder = cardholder;
	}

	public LocalDate getFechaVencimiento() {
		return fechaVencimiento;
	}

	public void setFechaVencimiento(LocalDate fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}

}
