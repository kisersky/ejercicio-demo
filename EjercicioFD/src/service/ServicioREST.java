package service;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import model.Operacion;
import model.Tarjeta;
import model.TarjetaPERE;
import model.TarjetaSCO;
import model.TarjetaSQUA;

@Path("/")
public class ServicioREST {

	@GET
	@Path("/infoOperacion/{marca}/{importe}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response operacionREST(@PathParam("marca") String marca, @PathParam("importe") String importe) {

		Tarjeta tarjeta;

		switch (marca) {
		case "SQUA":
			tarjeta = new TarjetaSQUA(null, null, null);
			break;
		case "SCO":
			tarjeta = new TarjetaSCO(null, null, null);
			break;
		case "PERE":
			tarjeta = new TarjetaPERE(null, null, null);
			break;
		default:
			ErrorResponse error = new ErrorResponse("La marca ingresada no existe");
			return Response.status(500).entity(error).build();
		}

		Double importeDouble;

		try {
			importeDouble = Double.valueOf(importe);
		} catch (Exception e) {
			ErrorResponse error = new ErrorResponse("El importe ingresado debe ser un número");
			return Response.status(500).entity(error).build();
		}

		Operacion operacion = new Operacion(tarjeta, importeDouble);

		if (operacion.esValida()) {
			Double importeFinal = operacion.obtenerImporteConTasa();
			MensajeImporte mensaje = new MensajeImporte(marca, String.format("%.2f", importeFinal));

			return Response.status(200).entity(mensaje).build();
		} else {
			ErrorResponse error = new ErrorResponse(
					"La operación no es válida. El importe no puede superar los $1000.");
			return Response.status(500).entity(error).build();
		}
	}

	@GET
	@Path("/verify")
	@Produces(MediaType.TEXT_PLAIN)
	public Response verifyRESTService() {
		String result = "El servicio está corriendo";

		return Response.status(200).entity(result).build();
	}

}
