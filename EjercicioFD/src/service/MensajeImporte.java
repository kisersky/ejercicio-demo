package service;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "mensajeImporte")
public class MensajeImporte {
	private String marca;
	private String importe;

	public MensajeImporte(String marca, String importe) {
		this.setMarca(marca);
		this.importe = importe;
	}
	
	protected MensajeImporte() {
		
	}

	public String getImporte() {
		return importe;
	}

	public void setImporte(String importe) {
		this.importe = importe;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}
}
