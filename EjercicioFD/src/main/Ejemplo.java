package main;

import java.time.LocalDate;

import model.Operacion;
import model.Tarjeta;
import model.TarjetaPERE;
import model.TarjetaSCO;

public class Ejemplo {
	public static void main(String[] args) {
		Tarjeta tarjetaSCO = new TarjetaSCO(1234567890123456L, "Juan Pérez", LocalDate.of(2022, 01, 01));
		Tarjeta tarjetaPERE = new TarjetaPERE(9876543210987654L, "María Pérez", LocalDate.of(2026, 06, 01));
		Operacion operacion = new Operacion(tarjetaSCO, 800.0);

		boolean esOperacionValida = operacion.esValida();
		boolean esTarjetaValida = tarjetaSCO.esValida();
		boolean sonDistintas = tarjetaSCO.esDistintaA(tarjetaPERE);

		tarjetaSCO.imprimirInformacion();
		tarjetaPERE.imprimirInformacion();

		System.out.printf("¿Es válida la operación?: %s\n", esOperacionValida ? "Sí" : "No");
		System.out.printf("¿Es válida la tarjeta?: %s\n", esTarjetaValida ? "Sí" : "No");
		System.out.printf("¿Son distintas las tarjetas?: %s\n", sonDistintas ? "Sí" : "No");

		operacion.imprimirInformacion();
		operacion.cobrar();
	}
}
